#Persistent
#WinActivateForce
#SingleInstance, force
#NoEnv
SetBatchLines, -1
SetKeyDelay, 0, 25, Play
SetControlDelay, -1
SetWinDelay, -1
ListLines, Off
SetFormat, Float, 0.1
SetFormat, Integer, D
DetectHiddenWindows, On
CoordMode, Mouse, Relative
#include lib\updates.ahk
;====================================
;@Ahk2Exe-Obey type, = "%A_IsUnicode%" ? "Unicode" : "ANSI"
;@Ahk2Exe-Let version = 1.1.1.1
;@Ahk2Exe-AddResource %A_ScriptDir%\res\skin.bmp, 100
;@Ahk2Exe-AddResource *14 %A_ScriptDir%\res\ReplyEx.ico, 159
;@Ahk2Exe-AddResource *14 %A_ScriptDir%\res\ReplyEx.ico, 228
;@Ahk2Exe-Set Comments, Created in AutoHotkey 1.0.48.05. Compiled in %A_AhkVersion% %U_type%.
;@Ahk2Exe-SetCompanyName Drugwash Hobby Programming
;@Ahk2Exe-SetDescription ReplyEx - Creates reply-to-reply URL for WordPress blogs
;@Ahk2Exe-Set FileVersion, %U_version%
;@Ahk2Exe-SetInternalName ReplyEx.ahk
;@Ahk2Exe-SetCopyright Copyright � Drugwash`, Nov 2012-August 2023
;@Ahk2Exe-SetLegalTrademarks This software is released under the terms of the GNU General Public License
;@Ahk2Exe-SetOrigFilename ReplyEx.exe
;@Ahk2Exe-SetProductName ReplyEx
;@Ahk2Exe-Set ProductVersion, 1.1.0.0
;@Ahk2Exe-SetMainIcon %A_ScriptDir%\res\ReplyEx.ico
;@Ahk2Exe-UpdateManifest 0, Drugwash.HobbyProgramming.ReplyEx, %U_version%
;====================================
appname=ReplyEx
version=1.1.1.1
releaseD=August 22, 2023
releaseT := (A_IsUnicode ? "(Unicode)" : "(ANSI)") " public"
iconlocal = %A_ScriptDir%\res\%appname%.ico
skinlocal = %A_ScriptDir%\res\skin.bmp
debug=0
;====================================
sh=1
if !hILS := ILC_List("2", (A_IsCompiled ? A_ScriptFullPath : skinlocal), 100, 0)
	msgbox, Can't build ImageList
IniRead, pos, %appname%_preferences.ini, Settings, Position, % "x" A_ScreenWidth-40 " y33"
IniRead, skin, %appname%_preferences.ini, Settings, Skin, 8

if !A_IsCompiled
	Menu, Tray, Icon, %iconlocal%
Menu, Tray, Tip, %appname%
Menu, Tray, NoStandard
Menu, Tray, Add, Hide panel, GuiContextMenu
Menu, Tray, Default, Hide panel
Menu, Tray, Add
Menu, Tray, Add, Options, optshow
Menu, Tray, Add, About, about
Menu, Tray, Add
Menu, Tray, Add, Exit, save

Gui, Margin, 0, 0
Gui, Color, White, White
Gui,  +ToolWindow -Caption +Border +AlwaysOnTop
Gui, Font, s7 w400, Tahoma
Gui, Add, Button, x4 y4 w24 h24 0x80 hwndhBtn gon, Reply
Gui, Add, Picture, x0 y0 w32 h32 +0x400000E hwndhSkin gmoveit,
hbmp := ILC_FitBmp(hSkin, hILS, skin)
hbmp3 := ILC_FitBmp(hBtn, hILS, 20)
Gui, Show, %pos% w32 h32, %appname%
WinGet, hMain, ID, %appname%
hNextClip := DllCall("SetClipboardViewer", Ptr, hMain)
Gui, 2:Color, White, White
Gui, 2:Margin, 5, 5
Gui, 2:Font, s7, Wingdings
Gui, 2:Add, Text, x5 y0 w8 h10 0x200 Center BackgroundTrans, � ; Chr(234) won't work in _L
Gui, 2:Font
Gui, 2:Add, Picture, x5 y10 w256 h20 0xE hwndhSelect gselect,
Gui, 2:Add, Button, x+2 yp w40 hp, OK
Gui, 2:Show, Hide, %appname% options
WinGet, hSet, ID, %appname% options
if A_IsCompiled
	hbmp2 := DllCall("LoadImage"
				, Ptr		, DllCall("GetModuleHandle", Ptr, NULL, Ptr)
				, "UInt"	, 100
				, "UInt"	, 0
				, "Int"	, 256
				, "Int"	, 20
				, "UInt"	, 0x2000
				, Ptr)
else hbmp2 := DllCall("LoadImage"
				, Ptr		, NULL
				, "Str"	, skinlocal
				, "UInt"	, 0
				, "Int"	, 256
				, "Int"	, 20
				, "UInt"	, 0x2010
				, Ptr)
if hP := SetImage(hSelect, hbmp2)
	DllCall("DeleteObject", Ptr, hP)
hCursM := DllCall("LoadCursor", Ptr, NULL, "Int", 32646, Ptr)	; IDC_SIZEALL
hCursH := DllCall("LoadCursor", Ptr, NULL, "Int", 32649, Ptr)	; IDC_HAND
OnMessage(0x30D, "clipchg")		; WM_CHANGECBCHAIN
OnMessage(0x308, "clipdraw")	; WM_DRAWCLIPBOARD
OnMessage(0x200, "msmove")	; WM_MOUSEMOVE
return
;#########################################################
GuiContextMenu:
sh := !sh
Gui, % (sh ? "Show" : "Hide")
Menu, Tray, Rename, % (sh ? "Show" : "Hide") " panel", % (sh ? "Hide" : "Show") " panel"
return
;#########################################################
save:
WinGetPos, wx, wy,,, %appname%
Gui, Destroy
Gui, 2:Destroy
IniWrite, x%wx% y%wy%, %appname%_preferences.ini, Settings, Position
IniWrite, %skin%, %appname%_preferences.ini, Settings, Skin
DllCall("DeleteObject", Ptr, hbmp)
DllCall("DeleteObject", Ptr, hbmp2)
DllCall("DeleteObject", Ptr, hbmp3)
;DllCall("DestroyCursor", Ptr, hCursH)
;DllCall("DestroyCursor", Ptr, hCursM)
DllCall("ChangeClipboardChain", Ptr, hMain, Ptr, hNextClip)
ExitApp
;#########################################################
moveit:
DllCall("SetCursor", Ptr, hCursM)
PostMessage, 0xA1, 2,,, A	; WM_NCLBUTTONDOWN
return
;#########################################################
optshow:
Gui, 2:Show
return
;#########################################################
select:
MouseGetPos, mx, my, winid, ctrl, 2
ControlGetPos, cX, cY,,, Static2, ahk_id %hSet%
skin := 1+(mx-cX)//8
hbmp := ILC_FitBmp(hSkin, hILS, skin)
return
;#########################################################
on:
on := !on
as := on ? GetKeyState("Control", "P") : 0
hbmp3 := ILC_FitBmp(hBtn, hILS, on ? "2" : "20")
return
;#########################################################
OnClipChange:
if !on
	return
strg := Clipboard
if !InStr(strg, "#comment-")
	return
	rep := (as ? "&" : "?") "replytocom="
on := as := 0
;StringReplace, strg, strg, #comment-, &replytocom=
StringReplace, strg, strg, #comment-, %rep%
strg .= "#respond"
if debug
	msgbox, old: %Clipboard%`nnew: %strg%
Clipboard := strg
hbmp3 := ILC_FitBmp(hBtn, hILS, 20)
return
;#########################################################
2GuiClose:
2ButtonOK:
Gui, 2:Hide
return
;#########################################################
about:
n := A_IsUnicode ? "C" : "Not c"
MsgBox, 0x43040, %appname%,
(
� Drugwash, %releaseD%
version %version% %releaseT%
%n%ompatible with x64 systems.

Drag border with the mouse to position.
Right-click to hide/Dbl-click icon to show.

Usage (only on WordPress blogs):
- click the blue button (turns red) [? mode] OR
- Ctrl+click the blue button (turns red) [& mode];
- copy the reply URL to clipboard
(usually it's under post date & time)
- paste in Address Bar and Go
)
return
;#########################################################
msmove(wP, lP, msg, hwnd)
{
Global Ptr, hSet, hSelect, hCursH
Static hide
MouseGetPos, mx, my, winid, ctrl, 2
if (winid != hSet)
	return
if (ctrl != hSelect)
	{
	if !hide
		{
		GuiControl, 2:Hide, Static1
		hide=TRUE
		}
	return
	}
ControlGetPos, X, Y,,, Static1, ahk_id %hSet%
ControlGetPos, cX, cY,,, Static2, ahk_id %hSet%
s := (mx-cX)//8
if (X != cX+8*s)
	{
	ControlMove, Static1, % cX+8*s,,,, ahk_id %hSet%
	GuiControl, 2:Show, Static1
	hide=
	}
DllCall("SetCursor", Ptr, hCursH)
}
;#########################################################
SetImage(hCtrl, hImg, type=0)
{
Global Ptr
Static ImgTypes="BIC012"
t := SubStr(ImgTypes, InStr(ImgTypes, type) + 3*(InStr(ImgTypes, type)<4), 1)
if t not between 0 and 2
	return
WinGetClass, cls, ahk_id %hCtrl%
return DllCall("SendMessage"
			, Ptr		, hCtrl
			, "UInt"	, (cls="Button" ? 0xF7 : 0x172)
			, "UInt"	, t
			, Ptr		, hImg
			, Ptr)
}
; BM_SETIMAGE=0xF7, STM_SETIMAGE=0x172
; IMAGE_BITMAP=0, IMAGE_ICON=1, IMAGE_CURSOR=2
;#########################################################
clipchg(wP, lP, msg, hwnd)
{
Global
if (lP=hNextClip)
	hNextClip := lP
else SendMessage, msg, wP, lP,, ahk_id %hNextClip%
return hNextClip ? FALSE : TRUE
}

clipdraw(wP, lP, msg, hwnd)
{
Global
SendMessage, msg, wP, lP,, ahk_id %hNextClip%
SetTimer, OnClipChange, -5
return FALSE
}
;#########################################################
#include lib\func_ImageList.ahk
