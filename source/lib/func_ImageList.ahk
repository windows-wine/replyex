; � Drugwash, February-November 2012
; Set of ImageList functions that allow custom icon sizes
; Many thanks to SKAN for fixing bitmap stretch issue under XP

; default: 16x16 ILC_COLOR24 ILC_MASK
; M=ILC_MASK P=ILC_PALETTE D=ILC_COLORDDB accepted colors: 4 8 16 24 32

ILC_Create(i, g="1", s="16x16", f="M24")
{
if i<1
	return 0
StringSplit, s, s, x
s2 := s2 ? s2 : s1
c=
Loop, Parse, f
	if A_LoopField is digit
		c .= A_LoopField
StringReplace, f, f, c,,
m := c|(InStr(f, "M") ? 0x1 : 0)|(InStr(f, "P") ? 0x800 : 0)|(InStr(f, "D") ? 0xFE : 0)
return DllCall("ImageList_Create", "Int", s1, "Int", s2, "UInt", m, "Int", i, "Int", g)	; ILC_COLOR24 ILC_MASK
}

ILC_List(cx, file, idx="100", cd="1")	; cd=color depth 32bit, set 0 for 24bit or lower
{
Global Ptr, AW
mask := cd ? 0xFF000000 : 0xFFFFFFFF
Loop, %file%
	if A_LoopFileExt in exe,dll
		{
		if !hInst := DllCall("GetModuleHandle", "Str", file)
			hL := hInst := DllCall("LoadLibrary" AW, "Str", file)
		if idx is not integer
			i := &idx
		else i := idx
		hIL := DllCall("ImageList_LoadImage" AW, Ptr, hInst, "UInt", i, "Int", cx, "Int", 1, "UInt", mask, "UInt", 0, "UInt", 0x2000)
		}
	else if A_LoopFileExt in bmp
		hIL := DllCall("ImageList_LoadImage" AW, Ptr, 0, "Str", file, "Int", cx, "Int", 1, "UInt", mask, "UInt", 0, "UInt", 0x2010)
if (hInst && hL)
	DllCall("FreeLibrary", Ptr, hInst)
return hIL
}

ILC_FitBmp(hPic, hIL, idx="1")
{
Static
Global Ptr
PtrSize := A_PtrSize ? A_PtrSize : 4
cif := 0x8	; CopyImage flags (0x2000 has issues in Win10)
WinGetPos,,, W1, H1, ahk_id %hPic%
if (W1 && H1)
	W := W1, H := H1
VarSetCapacity(IMAGEINFO, 24+2*PtrSize, 0)
DllCall("ImageList_GetImageInfo", Ptr, hIL, "Int", idx-1, Ptr, &IMAGEINFO)
bx := NumGet(IMAGEINFO, 8+2*PtrSize, "Int")
by := NumGet(IMAGEINFO, 12+2*PtrSize, "Int")
bw := NumGet(IMAGEINFO, 16+2*PtrSize, "Int")-bx
bh := NumGet(IMAGEINFO, 20+2*PtrSize, "Int")-by
;GuiControl,, Static1, Image %idx% @ %bx%.%by% size %bw%x%bh%
hDC := DllCall("CreateCompatibleDC", Ptr, 0)
hBm1 := DllCall("CreateBitmap" , "Int", bw, "Int", bh, "UInt", 1, "UInt", 0x18, "UInt", 0)
hBmp2 := DllCall("CopyImage", Ptr, hBm1, "UInt", 0, "Int", 0, "Int", 0, "UInt", cif, Ptr)	; 0=IMAGE_BITMAP
DllCall("SelectObject", Ptr, hDC, Ptr, hBmp2)
DllCall("ImageList_Draw", Ptr, hIL, "Int", idx-1, Ptr, hDC, "Int", 0, "Int", 0, "UInt", 0x0) ; ILD_NORMAL
DllCall("DeleteObject", Ptr, hBm1)
DllCall("DeleteDC", Ptr, hDC)
hBmp := DllCall("CopyImage", Ptr, hBmp2, "UInt", 0, "Int", W, "Int", H, "UInt", cif, Ptr)	; 0=IMAGE_BITMAP
WinGetClass, cls, ahk_id %hPic%	; according to control class, use BM_SETIMAGE or STM_SETIMAGE, IMAGE_BITMAP
if hP := DllCall("SendMessage", Ptr, hPic, "UInt", (cls="Button" ? 0xF7 : 0x172), "UInt", 0, Ptr, hBmp)
	DllCall("DeleteObject", Ptr, hP)
DllCall("DeleteObject", Ptr, hBmp2)
return hBmp
}

ILC_Destroy(hwnd)
{
Global Ptr
return DllCall("ImageList_Destroy", Ptr, hwnd)
}

; LR_CREATEDIBSECTION=0x2000 LR_LOADFROMFILE=0x10 LR_LOADTRANSPARENT=0x20 LR_SHARED=0x8000
; IMAGE_BITMAP=0x0 IMAGE_ICON=0x1 IMAGE_CURSOR=0x2
ILC_Add(hIL, icon, idx="1")
{
Global Ptr
Static it="BIC"
StringLeft, t, icon, 1
StringTrimLeft, icon, icon, 1
Loop, Parse, it
	if (A_LoopField=t)
		{
		t := A_Index-1
		break
		}
hInst=0
if icon is integer
	hIcon := icon
else
	{
	Loop, %icon%
		if A_LoopFileExt in exe,dll
			{
			if !hInst := DllCall("GetModuleHandle", "Str", icon)
				hL := hInst := DllCall("LoadLibrary", "Str", icon)
			flags=0x2000
		; need to use MAKEINTRESOURCE here
			if idx is not integer
				i := &idx
			else i := idx
			hIcon := DllCall("LoadImage", Ptr, hInst, "UInt", i, "UInt", t, "Int", 0, "Int", 0, "UInt", flags)
			}
		else if A_LoopFileExt in bmp,ico,cur,ani
			{
			flags=0x2010
			hIcon := DllCall("LoadImage", Ptr, hInst, "Str", icon, "UInt", t, "Int", 0, "Int", 0, "UInt", flags)
			}
		else
			{
			i := idx
			flags=0x8000
			hIcon := DllCall("LoadImage", Ptr, hInst, "UInt", i, "UInt", t, "Int", 0, "Int", 0, "UInt", flags)
			}
	}
if (hInst && hL)
	DllCall("FreeLibrary", Ptr, hInst)
if t=0
	{
	DllCall("ImageList_Add", Ptr, hIL, Ptr, hIcon, "UInt", 0)
	DllCall("DeleteObject", Ptr, hIcon)
	}
if t=1
	{
	DllCall("ImageList_ReplaceIcon", Ptr, hIL, "UInt", -1, Ptr, hIcon)
	DllCall("DestroyIcon", Ptr, hIcon)
	}
if t=2
	{
	DllCall("ImageList_ReplaceIcon", Ptr, hIL, "UInt", -1, Ptr, hIcon)
	DllCall("DestroyCursor", Ptr, hIcon)
	}
}
